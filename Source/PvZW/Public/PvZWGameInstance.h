// Free for anybody

#pragma once

#include "Engine/GameInstance.h"
#include "Runtime/MoviePlayer/Public/MoviePlayer.h"
#include "PvZWGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class PVZW_API UPvZWGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;

	UFUNCTION()
		virtual void BeginLoadingScreen(const FString& MapName);
	UFUNCTION()
		virtual void EndLoadingScreen();
	
};
