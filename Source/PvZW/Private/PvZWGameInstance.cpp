// Free for anybody

#include "PvZW.h"
#include "PvZWGameInstance.h"




void UPvZWGameInstance::Init()
{
	UGameInstance::Init();

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UPvZWGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMap.AddUObject(this, &UPvZWGameInstance::EndLoadingScreen);
}

void UPvZWGameInstance::BeginLoadingScreen(const FString& MapName)
{
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = true;
		LoadingScreen.bMoviesAreSkippable = false;
		LoadingScreen.MinimumLoadingScreenDisplayTime = 1;
		//LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();
		LoadingScreen.PlaybackType = EMoviePlaybackType::MT_Looped;
		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
}

void UPvZWGameInstance::EndLoadingScreen()
{

}