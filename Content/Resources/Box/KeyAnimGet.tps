<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>F:/Cloud/Mega.pro/Projects/Wedding/PVZ2/Present/KeyAnim/KeyAnimGet.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unreal-paper2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>KeyAnimGet.paper2dsprites</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">MovieClip-Get_0.png</key>
            <key type="filename">MovieClip-Get_1.png</key>
            <key type="filename">MovieClip-Get_10.png</key>
            <key type="filename">MovieClip-Get_11.png</key>
            <key type="filename">MovieClip-Get_12.png</key>
            <key type="filename">MovieClip-Get_13.png</key>
            <key type="filename">MovieClip-Get_14.png</key>
            <key type="filename">MovieClip-Get_15.png</key>
            <key type="filename">MovieClip-Get_16.png</key>
            <key type="filename">MovieClip-Get_17.png</key>
            <key type="filename">MovieClip-Get_18.png</key>
            <key type="filename">MovieClip-Get_19.png</key>
            <key type="filename">MovieClip-Get_2.png</key>
            <key type="filename">MovieClip-Get_20.png</key>
            <key type="filename">MovieClip-Get_21.png</key>
            <key type="filename">MovieClip-Get_22.png</key>
            <key type="filename">MovieClip-Get_23.png</key>
            <key type="filename">MovieClip-Get_24.png</key>
            <key type="filename">MovieClip-Get_25.png</key>
            <key type="filename">MovieClip-Get_26.png</key>
            <key type="filename">MovieClip-Get_27.png</key>
            <key type="filename">MovieClip-Get_28.png</key>
            <key type="filename">MovieClip-Get_29.png</key>
            <key type="filename">MovieClip-Get_3.png</key>
            <key type="filename">MovieClip-Get_30.png</key>
            <key type="filename">MovieClip-Get_31.png</key>
            <key type="filename">MovieClip-Get_32.png</key>
            <key type="filename">MovieClip-Get_33.png</key>
            <key type="filename">MovieClip-Get_34.png</key>
            <key type="filename">MovieClip-Get_35.png</key>
            <key type="filename">MovieClip-Get_36.png</key>
            <key type="filename">MovieClip-Get_37.png</key>
            <key type="filename">MovieClip-Get_38.png</key>
            <key type="filename">MovieClip-Get_39.png</key>
            <key type="filename">MovieClip-Get_4.png</key>
            <key type="filename">MovieClip-Get_40.png</key>
            <key type="filename">MovieClip-Get_41.png</key>
            <key type="filename">MovieClip-Get_42.png</key>
            <key type="filename">MovieClip-Get_43.png</key>
            <key type="filename">MovieClip-Get_44.png</key>
            <key type="filename">MovieClip-Get_45.png</key>
            <key type="filename">MovieClip-Get_46.png</key>
            <key type="filename">MovieClip-Get_47.png</key>
            <key type="filename">MovieClip-Get_48.png</key>
            <key type="filename">MovieClip-Get_49.png</key>
            <key type="filename">MovieClip-Get_5.png</key>
            <key type="filename">MovieClip-Get_50.png</key>
            <key type="filename">MovieClip-Get_51.png</key>
            <key type="filename">MovieClip-Get_52.png</key>
            <key type="filename">MovieClip-Get_53.png</key>
            <key type="filename">MovieClip-Get_54.png</key>
            <key type="filename">MovieClip-Get_55.png</key>
            <key type="filename">MovieClip-Get_56.png</key>
            <key type="filename">MovieClip-Get_57.png</key>
            <key type="filename">MovieClip-Get_58.png</key>
            <key type="filename">MovieClip-Get_59.png</key>
            <key type="filename">MovieClip-Get_6.png</key>
            <key type="filename">MovieClip-Get_7.png</key>
            <key type="filename">MovieClip-Get_8.png</key>
            <key type="filename">MovieClip-Get_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,56,113,112</rect>
                <key>scale9Paddings</key>
                <rect>56,56,113,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>MovieClip-Get_0.png</filename>
            <filename>MovieClip-Get_1.png</filename>
            <filename>MovieClip-Get_2.png</filename>
            <filename>MovieClip-Get_3.png</filename>
            <filename>MovieClip-Get_4.png</filename>
            <filename>MovieClip-Get_5.png</filename>
            <filename>MovieClip-Get_6.png</filename>
            <filename>MovieClip-Get_7.png</filename>
            <filename>MovieClip-Get_8.png</filename>
            <filename>MovieClip-Get_9.png</filename>
            <filename>MovieClip-Get_10.png</filename>
            <filename>MovieClip-Get_11.png</filename>
            <filename>MovieClip-Get_12.png</filename>
            <filename>MovieClip-Get_13.png</filename>
            <filename>MovieClip-Get_14.png</filename>
            <filename>MovieClip-Get_15.png</filename>
            <filename>MovieClip-Get_16.png</filename>
            <filename>MovieClip-Get_17.png</filename>
            <filename>MovieClip-Get_18.png</filename>
            <filename>MovieClip-Get_19.png</filename>
            <filename>MovieClip-Get_20.png</filename>
            <filename>MovieClip-Get_21.png</filename>
            <filename>MovieClip-Get_22.png</filename>
            <filename>MovieClip-Get_23.png</filename>
            <filename>MovieClip-Get_24.png</filename>
            <filename>MovieClip-Get_25.png</filename>
            <filename>MovieClip-Get_26.png</filename>
            <filename>MovieClip-Get_27.png</filename>
            <filename>MovieClip-Get_28.png</filename>
            <filename>MovieClip-Get_29.png</filename>
            <filename>MovieClip-Get_30.png</filename>
            <filename>MovieClip-Get_31.png</filename>
            <filename>MovieClip-Get_32.png</filename>
            <filename>MovieClip-Get_33.png</filename>
            <filename>MovieClip-Get_34.png</filename>
            <filename>MovieClip-Get_35.png</filename>
            <filename>MovieClip-Get_36.png</filename>
            <filename>MovieClip-Get_37.png</filename>
            <filename>MovieClip-Get_38.png</filename>
            <filename>MovieClip-Get_39.png</filename>
            <filename>MovieClip-Get_40.png</filename>
            <filename>MovieClip-Get_41.png</filename>
            <filename>MovieClip-Get_42.png</filename>
            <filename>MovieClip-Get_43.png</filename>
            <filename>MovieClip-Get_44.png</filename>
            <filename>MovieClip-Get_45.png</filename>
            <filename>MovieClip-Get_46.png</filename>
            <filename>MovieClip-Get_47.png</filename>
            <filename>MovieClip-Get_48.png</filename>
            <filename>MovieClip-Get_49.png</filename>
            <filename>MovieClip-Get_50.png</filename>
            <filename>MovieClip-Get_51.png</filename>
            <filename>MovieClip-Get_52.png</filename>
            <filename>MovieClip-Get_53.png</filename>
            <filename>MovieClip-Get_54.png</filename>
            <filename>MovieClip-Get_55.png</filename>
            <filename>MovieClip-Get_56.png</filename>
            <filename>MovieClip-Get_57.png</filename>
            <filename>MovieClip-Get_58.png</filename>
            <filename>MovieClip-Get_59.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
